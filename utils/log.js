var log4js = require('log4js');
var LOG_LEVEL = 'DEBUG';
var avalLevels = [
	'TRACE',
	'DEBUG',
	'INFO',
	'WARN',
	'ERROR',
	'FATAL'
];

exports.setLogLevel = logLevel => {
	if(!~avalLevels.indexOf(logLevel)){
		console.log(`Cant set up log level: ${logLevel}. Staying ${LOG_LEVEL}`);
		return false;
	}

	LOG_LEVEL = logLevel;
	return true;
};

exports.createLogger = moduleName => {
	var curLogger = log4js.getLogger(moduleName);
	curLogger.setLevel(LOG_LEVEL);

	return curLogger;
};
