var os = require("os");
var commonConfig = require('confs/config.json');

var hostname = os.hostname();
var devHosts = ['rotten'];

var ENV = !~devHosts.indexOf(hostname) ? 'production' : 'development';

exports.getDALConf = () => {
	return commonConfig[ENV].dals;
};
