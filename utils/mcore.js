var DAL = require('controllers/DAL');
var logMod = require('./log');
var log = logMod.createLogger('MCORE');

exports.init = new Promise((resolve, reject) => {
	logMod.setLogLevel('DEBUG');
	DAL.init(err => {
		if(err){
			log.error(`An error caughr in mcore module: ${err}`);
			reject(err);
			return;
		}

		resolve();
	});
});