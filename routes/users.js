var express = require('express');
var router = express.Router();

var authProc = require('controllers/auth');
var log = require('log').createLogger('ROUTE_USERS');

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

router.get('/regUserHash', (req, res, next) => {
	authProc.checkRegisterHash(req.query, (err, resp) => {
		if(err){
			log.error('Error_regUserHash_0', err);
			res.send(err);
			return;
		}

		res.json(resp);
	});
});

module.exports = router;
