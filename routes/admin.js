var express = require('express');
var url = require('url');

var router = express.Router();
var authProc = require('controllers/auth');
var log = require('log').createLogger('ROUTE_ADMIN');

router.get('/', (req, res, next) => {
	res.end('admin testing root');
});

router.get('/addNewAdmin', function(req, res, next) {
	if(!req.query.userInfo){
		log.error('ERROR_route_addNewAdmin_0', 'Wrong params', req.query);
		res.end('wrong_params');
		return;
	}

	try{
		req.query.userInfo = JSON.parse(req.query.userInfo);
	}
	catch(e){
		log.error('ERROR_route_addNewAdmin_1', 'Can\'t parse params', req.query);
		res.end('internal_error');
		return;
	}

	authProc.addNewUser(req.query, (err, result) => {
		if(err){
			log.error(`An error occured: ${err}`);
			res.end(`INTERNAL_ERROR`);
			return;
		}

		res.json(result);
	});
});

module.exports = router;
