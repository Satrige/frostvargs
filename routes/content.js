var express = require('express');
var url = require('url');

var router = express.Router();
var contentProc = require('controllers/content');
var log = require('log').createLogger('ROUTE_CONTENT');

router.get('/', (req, res, next) => {
	res.end('content testing root');
});

router.get('/task/add', (req, res, next) => {
	contentProc.addTask(req.query, (err, result) => {
		if(err){
			log.error(`An error occured: ${err}`);
			res.end(`INTERNAL_ERROR`);
			return;
		}

		res.json({
			res: 'ok',
			result: result
		});
	});
});

router.get('/task/get', (req, res, next) => {
	contentProc.getTask(req.query, (err, result) => {
		if(err){
			log.error(`An error occured: ${err}`);
			res.end(`INTERNAL_ERROR`);
			return;
		}

		res.json({
			res: 'ok',
			result: result
		});
	});
});

module.exports = router;
