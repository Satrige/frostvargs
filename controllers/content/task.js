var _ = require('lodash');
var Validator = require('jsonschema').Validator;
var v = new Validator();

var DAL = require('controllers/DAL');
var log = require('log').createLogger('CONTROLLER_CONTENT_TAKS');

var schemas = require('./schemas.json');

var add = (params, callback) => {
	if(!params || !v.validate(params, schemas.add_schema)){
		log.error('ERROR_add_0', 'Wrong params', params);
		return callback('wrong_params');
	}

	let tasksDal = DAL.open('tasks');
	let newTask = new tasksDal.Task(params);

	newTask.save()
	.then(
		res => {
			log.info('New task has been saved: ', res);
			callback(undefined, {
				_id: res._id
			});
		}
	)
	.catch(
		err => {
			log.error('Error_add_1', err);
			callback(err);
		}
	);
};

exports.add = add;

var get = (params, callback) => {
	if(!params || !params._id){
		log.error('ERROR_get_0', 'Wrong params', params);
		return callback('wrong_params');
	}

	let tasksDal = DAL.open('tasks');
	let newTask = new tasksDal.Task(params);

	let findParams = [{_id: params._id}];
	!params.needAnsw && findParams.push({answer: false});

	tasksDal.Task.findOne(...findParams)
	.then(
		task => {
			if(!task){
				log.error('Error_get_1', 'Can\'t fing task with such query', query);
				throw new Error('wrong_params');
			}

			callback(undefined, task);
		}
	)
	.catch(
		err => {
			log.error('Error_get_2', err);
			callback(err);
		}
	);
};

exports.get = get;
