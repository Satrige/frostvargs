var log = require('log').createLogger('CONTROLLER_CONTENT_INDEX');

var taskMod = require('./task');

exports.addTask = (params, callback) => {
	taskMod.add(params, callback);
};

exports.getTask = (params, callback) => {
	taskMod.get(params, callback);
};