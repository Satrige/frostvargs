var dalConfig = require('config').getDALConf();
var redis = require('redis');
var log = require('log').createLogger('DAL_USERS_REDIS');

var authHash = 'regEmail';

var client = redis.createClient(dalConfig.redis);
client.on('error', err => {
	log.error('ERROR_USERS_REDIS_0', err);
});

exports.setUpMailHash = params => {
	return new Promise((resolve, reject) => {
		client.hset(authHash, params.hash, params.email, (err, res) => {
			if(err){
				log.error('ERROR_setUpMailHash_0', 'Can\'t store hash with email', params, err);
				reject(err);
				return;
			}

			resolve(undefined);
		});
	});
};

exports.getMailHash = hashVal => {
	return new Promise((resolve, reject) => {
		log.info(`hashVal: ${hashVal}`);
		client.hget(authHash, hashVal, (err, email) => {
			if(err){
				log.error('ERROR_getMailHash_0', 'Can\'t retrieve email by hash', hashVal, err);
				reject(err);
				return;
			}

			resolve(email);
		});
	});
};

exports.removeMailHash = hashVal => {
	return new Promise((resolve, reject) => {
		client.hdel(authHash, hashVal, err => {
			if(err){
				log.error('ERROR_removeMailHahs_0', 'Can\'t delete value from authHash', hashVal, err);
				reject(err);
				return;
			}

			resolve(undefined);
		});
	});
};