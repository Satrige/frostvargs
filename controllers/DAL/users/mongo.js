var mongoose = require('mongoose');
var log = require('log').createLogger('DAL_USERS_MONGO');

var userSchema = mongoose.Schema({
	name: String,
	surname: String,
	email: {
		required: true,
		type: String
	},
	password: String,
	isAdmin: Boolean,
	validated: {
		type: Boolean,
		default: false
	}
}, {
	collection: 'users'
});

var User = mongoose.model('User', userSchema);

exports.User = User;

exports.getUser = query => {
	return new Promise((resolve, reject) => {
		User.findOne(query)
		.exec((err, user) => {
			log.info('err: ', err);
			log.info('user: ', user);

			if(err){
				log.error('Error_getUser_0', err);
				reject(err);
				return;
			}

			resolve(user);
		});
	});
};
