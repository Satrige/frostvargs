var _ = require('lodash');
var mongoose = require('mongoose');
var fs = require('fs');

var log = require('log').createLogger('DAL_INDEX');
var dalConfig = require('config').getDALConf();

mongoose.connect(`mongodb://${dalConfig.db.host}/${dalConfig.db.dbName}`);

class DalsList{
	constructor(){
		this.list = {};
	}

	open(name){
		var engines = this.list[name];
		if(!engines){
			log.error(`Can't find dal: ${name}`);
			throw new Error(`Can't find dal: ${name}`);
		}

		let dalMethods = {};

		_.forEach(engines, curEngine => {
			for(let i in curEngine){
				if(_.isFunction(curEngine[i])){
					dalMethods[i] = curEngine[i];
				}
			}
		});

		return dalMethods;
	}

	addDal(params){
		this.list[params.name] = params.engines;
	}
}

var db = mongoose.connection;
var DALs = new DalsList();

exports.init = callback => {
	new Promise((resolve, reject) => {
		db.once('open', () => {
			log.info('Connection to database was established');
			resolve();
		});

		db.on('error', (err) => {
			log.error('An error occured while trying to connect to db: ', err);
			reject(err);
		});
	})
	.then(
		() => {
			let curFolder = 'controllers/DAL',
				files = fs.readdirSync(curFolder),
				dalDirs = [];

			_.forEach(files, filePath => {
				let absPath = `${curFolder}/${filePath}`;
				fs.lstatSync(absPath).isDirectory() && dalDirs.push({
					absPath: absPath,
					name: filePath
				});
			});

			return dalDirs;
		}
	)
	.then(
		dalDirs => {
			_.forEach(dalDirs, dalDir => {
				let allEngines = _.map(fs.readdirSync(dalDir.absPath), singleEngine => {
					return require(`${dalDir.absPath}/${singleEngine}`);
				});

				DALs.addDal({
					name: dalDir.name,
					engines: allEngines
				});
			});

			log.info(`All DALs has been connected`);
			callback(undefined);
		}
	)
	.catch(
		err => {
			log.error('An error has caught: ', err);
			callback(err);
		}
	);
};

exports.open = name => {
	return DALs.open(name);
};
