var mongoose = require('mongoose');
var log = require('log').createLogger('DAL_TASKS_MONGO');

var taskSchema = mongoose.Schema({
	header: String,
	body: {
		required: true,
		type: String
	},
	answer: {
		required: true,
		type: String
	},
	type: {
		default: 'text',
		type: String
	}
}, {
	collection: 'tasks'
});

var Task = mongoose.model('Task', taskSchema);

exports.Task = Task;

