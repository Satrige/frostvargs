var log = require('log').createLogger('CONTROLLER_MAIL');

exports.sendRegisterMail = (params, callback) => {
	var text = '';
	return new Promise((resolve, reject) => {
		if(!params || !params.email || !params.hash){
			log.error('ERROR_sendRegisterMail_0', 'Wrong params', params);
			reject('wrong_params');
			return;
		}

		text = `Здравствуйте, для регистрации, пройдите, пожалуйста по ссылке:
			http://localhost:3000/users/regUserHash/?email=${params.email}&hash=${params.hash}`;

		log.info('Text of register letter: ', text);
		resolve();
	})
	.then(
		() => {
			return sendLetter({
				email: params.email,
				text: text,
				subject: 'Регистрация'
			});
		}
	);
};

var sendLetter = params => {
	return new Promise((resolve, reject) => {
		log.warn('sendLetter function is not implemented yet');
		resolve();
	});
};