var _ = require('lodash');
var Validator = require('jsonschema').Validator;
var v = new Validator();

var DAL = require('controllers/DAL');
var crypto = require('controllers/utils/crypto');
var mailProc = require('controllers/mail');
var log = require('log').createLogger('CONTROLLER_AUTH');

var schemas = require('./schemas.json');

var addNewUser = (params, callback) => {
	//TODO Add checking for email
	if(!params || !v.validate(params, schemas.addNewUser_schema)){
		log.error('ERROR_addNewUser_0', 'Wrong params', userInfo);
		return callback('wrong_params');
	}

	let userInfo = params.userInfo;
	let usersDal = DAL.open('users');

	let newUser = new usersDal.User(_.assign(userInfo, {
		isAdmin: true
	}));
	let email = newUser.email;
	let hash = '';

	new Promise((resolve, reject) => {
		newUser.save(err => {
			if(err){
				log.error('Error_addNewUser_2', err);
				reject(err);
				return;
			}

			resolve();
		});
	})
	.then(
		() => {
			hash = crypto.generateRandomHash(email);

			return usersDal.setUpMailHash({
				email: email,
				hash: hash
			});
		}
	)
	.then(
		() => {
			return mailProc.sendRegisterMail({
				email: email,
				hash: hash
			});
		}
	)
	.then(
		() => {
			callback(undefined, {
				res: 'ok'
			});
		}
	)
	.catch(
		err => {
			log.error('Error_addNewUser_2', err);
			callback(err);
		}
	);
};

exports.addNewUser = addNewUser;

var checkRegisterHash = (params, callback) => {
	if(!params || !params.hash || !params.email){
		log.error('Error_checkRegisterHash_0', 'Wrong params: ', params);
		callback('wrong_params');
		return;
	}

	let usersDal = DAL.open('users');

	usersDal
	.getMailHash(params.hash)
	.then(
		email => {
			if(email !== params.email){
				log.warn('Wrong email was sent with hash: ', email, params.email);
				throw new Error('wrong_email');
			}

			return usersDal.getUser({
				email: email
			});
		}
	)
	.then(
		user => {
			if(!user){
				log.error('There no user with such email in DB: ', params.email);
				throw new Error('wrong_params');
			}

			user.validated = true;
			return user.save();
		}
	)
	.then(
		() => {
			log.info(`User with email: ${params.email} has been validated.`);

			return usersDal.removeMailHash(params.hash);
		}
	)
	.catch(
		err => {
			log.error('Error_checkRegisterHash_1', err);
			callback(err);
		}
	);
};

exports.checkRegisterHash = checkRegisterHash;
