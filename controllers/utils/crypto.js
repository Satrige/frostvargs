var crypto = require('crypto');

var SEED = 'huiUgadaEsh';

var generateRandomHash = (val) => {
	let randStr = (new Date()).valueOf().toString() + '_' + val 
		+ '_' + SEED + '_ ' + Math.random().toString();

	return crypto.createHash('sha1').update(randStr).digest('hex');
};

exports.generateRandomHash = generateRandomHash;